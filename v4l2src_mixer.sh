gst-launch-1.0 -e videomixer name=mix ! videoconvert ! xvimagesink sync=false \
   v4l2src device=/dev/video0 ! videoscale ! video/x-raw,width=100,height=100 ! videobox top=-200 left=-200 ! mix. \
   videotestsrc pattern=0 ! video/x-raw,width=100,height=100,framerate=15/1 ! videobox top=-200 left=-100 ! queue ! mix. \
