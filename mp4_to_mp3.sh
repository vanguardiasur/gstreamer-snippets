#!/bin/bash
gst-launch filesrc location=$1 ! qtdemux name=demux \
demux. ! queue ! faad ! lamemp3enc quality=2 target=bitrate bitrate=192 cbr=true \
! id3v2mux ! filesink location=$1.mp3
