# Pipeline to h264 encode raw video from video4linux,
# then decode it and render it to screen.
# There's a bug with this particular video4linux driver (uvcvideo)
# and so I need to specify framerate explicitly.
# I need to sync=false on the video render or else it drops buffers.
#gst-launch-1.0 -v v4l2src ! video/x-raw,width=640,height=480,format=I420,framerate=15/1 ! \
#		  vaapih264enc ! vaapidecodebin ! queue ! \
#		  xvimagesink max-lateness=-1

# iostat shows ~3% CPU consumption on my i5 intel laptop!!!

# Let's save it to disk
gst-launch-1.0 -e v4l2src ! video/x-raw,width=640,height=480,framerate=15/1 ! \
		  vaapipostproc ! vaapih264enc ! mp4mux ! filesink location=test.mp4
# iostat shows ~2% CPU consumption, and "perf top" most important
# functions are kernel-land!

gst-launch-1.0 -e videotestsrc ! video/x-raw,width=640,height=480,framerate=15/1 ! \
		  vaapipostproc ! vaapih264enc ! mp4mux ! fakesink
