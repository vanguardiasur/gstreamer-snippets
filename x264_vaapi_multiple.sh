#for i in {1..16}; do
#	echo $i
#	gst-launch-1.0 videotestsrc pattern=ball is-live=true ! \
#	video/x-raw,width=720,height=576,framerate=30/1 ! \
#	vaapipostproc deinterlace-mode=disabled ! vaapipostproc ! queue ! \
#	vaapih264enc rate-control=cbr bitrate=300 ! queue ! \
#	avdec_h264 ! vaapipostproc ! ximagesink sync=false &
#done

VAAPISTREAM="t. ! queue ! vaapih264enc rate-control=cbr bitrate=300 ! avdec_h264 ! vaapisink sync=false"

gst-launch-1.0 v4l2src ! \
	video/x-raw,width=640,height=480,format=YUY2 ! vaapipostproc ! tee name=t \
	${VAAPISTREAM} \
	${VAAPISTREAM} \
	${VAAPISTREAM}

