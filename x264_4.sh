# This pipeline works on my i5
gst-launch-1.0 -v v4l2src norm=PAL-Nc ! video/x-raw,frame=1/1,width=720,height=576 ! deinterlace ! vaapipostproc ! vaapih264enc ! queue leaky=2 ! vaapidecodebin ! ximagesink sync=false

# This pipeline works as well!
gst-launch-1.0 -v v4l2src norm=PAL-Nc ! video/x-raw,width=720,height=576 ! \
                 deinterlace ! videoconvert ! x264enc ! \
		 avdec_h264 ! ximagesink

# Quick profiling:

# 1. raw display w/deinterlace 14% cpu w/top
gst-launch-1.0 -v v4l2src norm=PAL-Nc ! video/x-raw,width=720,height=576 ! deinterlace  ! videoconvert ! ximagesink

# 2. h264 enc/dec w/deinterlace 100% cpu w/top
gst-launch-1.0 -v v4l2src norm=PAL-Nc ! video/x-raw,width=720,height=576 ! deinterlace ! x264enc ! avdec_h264 ! videoconvert ! ximagesink sync=false

# 2. h264 enc/dec w/deinterlace, speed-preset ultrafast 35% cpu w/top
gst-launch-1.0 -v v4l2src norm=PAL-Nc ! video/x-raw,width=720,height=576 ! deinterlace ! x264enc speed-preset=ultrafast ! avdec_h264 ! videoconvert ! ximagesink sync=false

# 3. vaapi, note that there's some quirk needed in the deinterlacing
gst-launch-1.0 -v v4l2src norm=PAL-Nc ! video/x-raw,width=720,height=576 ! vaapipostproc deinterlace-mode=disabled ! vaapipostproc ! vaapih264enc ! vaapidecodebin ! ximagesink sync=false
