# Pipeline to h264 encode raw video from video4linux,
# then decode it and render it to screen.
# There's a bug with this particular video4linux driver (uvcvideo)
# and so I need to specify framerate explicitly.
#gst-launch-1.0 -v v4l2src ! video/x-raw,width=640,height=480,format=I420,framerate=15/1 ! \
#		  x264enc ! avdec_h264 ! \
#		  xvimagesink

# iostat shows ~30% CPU consumption on my i5 intel laptop.

gst-launch-1.0 -e v4l2src ! video/x-raw,width=640,height=480,framerate=15/1 ! \
		  x264enc ! mp4mux ! filesink location=sw.mp4
