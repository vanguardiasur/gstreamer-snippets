# You can test this one on X
true && gst-launch-1.0 -v videotestsrc name=video_src is-live=true ! \
	video/x-raw,width=720,height=480,framerate="15/1" ! \
	videorate ! videoconvert ! \
	tee name=t ! \
	videorate ! videoscale ! queue max-size-buffers=100 max-size-bytes=0 max-size-time=0 ! \
	video/x-raw,width=360,height=240,framerate="8/1" ! \
	jpegenc name=jpg quality=60 ! decodebin ! autovideosink \
	t. ! queue ! videoscale ! \
	video/x-raw,width=720,height=480,framerate="15/1" ! \
	x264enc key-int-max=30 ip-factor=0.5 speed-preset=veryfast bitrate=140 aud=false option-string="ratetol=inf" bframes=0 ! decodebin ! autovideosink \

return

# This is a possible bridge pipeline (medium quality on preview and video)
gst-launch-1.0 -v videotestsrc name=video_src is-live=true ! \
	video/x-raw,width=720,height=480,framerate="15/1" ! \
	videorate ! videoconvert ! \
	tee name=t ! \
	videorate ! videoscale ! queue max-size-buffers=100 max-size-bytes=0 max-size-time=0 ! \
	video/x-raw,width=360,height=240,framerate="8/1" ! \
	jpegenc name=jpg quality=60 ! fakesink \
	t. ! queue ! videoscale ! \
	video/x-raw,width=720,height=480,framerate="15/1" ! \
	x264enc key-int-max=30 ip-factor=0.5 speed-preset=veryfast bitrate=140 aud=false option-string="ratetol=inf" bframes=0 ! fakesink silent=false

# with v4l2src, the rate, size and pixel format are supported natively, thus
# this pipeline should be good too, i.e. removing videorate ! videoconvert
# before the tee
gst-launch-1.0 -v videotestsrc name=video_src is-live=true ! \
	video/x-raw,width=720,height=480,framerate="15/1" ! \
	tee name=t ! \
	videorate ! videoscale ! queue max-size-buffers=100 max-size-bytes=0 max-size-time=0 ! \
	video/x-raw,width=360,height=240,framerate="8/1" ! \
	jpegenc name=jpg quality=60 ! fakesink
	t. ! queue ! videoscale ! \
	video/x-raw,width=720,height=480,framerate="15/1" ! fakesink

# Also, note that we have removed do-timestamps. It should be
# better to work with progressive frames, but since we don't
# want to waste CPU on it.. let's use method=weave
gst-launch-1.0 -v videotestsrc name=video_src is-live=true ! \
	video/x-raw,width=720,height=480,framerate="15/1" ! \
	deinterlace method=weave ! \
	tee name=t ! \
	videorate ! videoscale ! queue max-size-buffers=100 max-size-bytes=0 max-size-time=0 ! \
	video/x-raw,width=360,height=240,framerate="8/1" ! \
	jpegenc name=jpg quality=60 ! fakesink
	t. ! queue ! videoscale ! videoconvert ! \
	video/x-raw,width=720,height=480,framerate="15/1" ! fakesink

# Rosko is concerned about jpegenc doing format conversion behind
# the curtains. I really think that would SUCK big time, but let me
# double check!

# So... how do I test performance?
# Draw a baseline on all my machines?
# Build bridge: 1.0.9 cpu with pipeline
# VM bridge: 1.0.9 cpu with pipeline
# Rosko bridge: 1.0.9 cpu with pipeline

gst-launch-1.0 -v videotestsrc name=video_src is-live=true ! video/x-raw,width=720,height=480,framerate="15/1" ! videorate ! videoconvert ! tee name=t ! videorate ! videoscale ! queue max-size-buffers=100 max-size-bytes=0 max-size-time=0 ! video/x-raw,width=360,height=240,framerate="8/1" ! jpegenc name=jpg quality=60 ! fakesink name=previewsink silent=true t. ! queue ! videoscale ! video/x-raw,width=720,height=480,framerate="15/1" ! x264enc key-int-max=30 ip-factor=0.5 speed-preset=veryfast bitrate=140 aud=false option-string="ratetol=inf" bframes=0 ! fakesink name=videosink silent=true

# Bridge h264 path only
gst-launch-1.0 -v videotestsrc name=video_src is-live=true ! video/x-raw,width=720,height=480,framerate="15/1" ! videorate ! videoconvert ! x264enc key-int-max=30 ip-factor=0.5 speed-preset=veryfast bitrate=140 aud=false option-string="ratetol=inf" bframes=0 ! fakesink name=videosink silent=true
