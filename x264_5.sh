# Encoding path on bridge/mpool (bridge must be off)
gst-launch-1.0 -v v4l2src norm=PAL-Nc ! video/x-raw,width=720,height=576 ! vaapipostproc deinterlace-mode=disabled ! vaapipostproc ! vaapih264enc ! video/x-h264,profile=baseline ! eensink streamtype=0 cameraid=0x1234

# Decoding path - for some reason vaapidecodebin is laggy,
# so I decode with CPU (but. still use vaapipostproc for X pixel format conversion)
gst-launch-1.0 -v eensrc cameraid=0x1234 .VIDEO ! h264parse ! avdec_h264 ! vaapipostproc ! ximagesink sync=false

