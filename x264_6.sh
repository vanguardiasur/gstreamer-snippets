# Let's compare h264 compression
gst-launch-1.0 -v v4l2src num-buffers=300 norm=PAL-Nc ! video/x-raw,width=720,height=576,framerate=25/1 ! vaapipostproc deinterlace-mode=disabled ! vaapipostproc ! vaapih264enc tune=1 rate-control=cbr keyframe-period=50 max-bframes=0 bitrate=300 ! video/x-h264,profile=high ! filesink location=test_vaapih264enc.raw

#
gst-launch-1.0 -v v4l2src num-buffers=300 norm=PAL-Nc ! video/x-raw,width=720,height=576,framerate=25/1 ! deinterlace ! videoconvert ! x264enc key-int-max=50 speed-preset=veryfast bitrate=300 bframes=0 ! video/x-h264,profile=high ! filesink location=test_x264enc.raw

gst-launch-1.0 -e v4l2src num-buffers=3000 ! video/x-raw,width=640,height=480,framerate=15/1 ! videoconvert ! x264enc key-int-max=50 speed-preset=veryfast bitrate=300 bframes=0 ! video/x-h264,profile=high ! mp4mux ! filesink location=test_x264enc.mp4

gst-launch-1.0 -e v4l2src num-buffers=3000 ! video/x-raw,width=640,height=480,framerate=15/1 ! videoconvert ! vaapih264enc tune=0 rate-control=cbr keyframe-period=50 max-bframes=0 bitrate=300 ! video/x-h264,profile=high ! mp4mux ! filesink location=test_x264enc.mp4


