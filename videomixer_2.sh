gst-launch-1.0 videomixer name=mix ! videoconvert ! autovideosink \
   multifilesrc location="pluto.jpg" caps="image/jpeg,framerate=1/1" ! jpegdec ! \
     textoverlay font-desc="Sans 26" text="Live from Pluto" halignment=left shaded-background=true auto-resize=false ! \
     videoconvert ! video/x-raw ! mix. \
   videotestsrc pattern=13 ! video/x-raw, framerate=10/1, width=350, height=250 ! \
     textoverlay font-desc="Sans 24" text="CAM3" valignment=top halignment=left shaded-background=true ! \
     videobox border-alpha=0 top=-200 left=-850 ! mix. \
   videotestsrc pattern="snow" ! video/x-raw, framerate=10/1, width=350, height=250 ! \
     textoverlay font-desc="Sans 24" text="CAM2" valignment=top halignment=left shaded-background=true ! \
     videobox border-alpha=0 top=-200 left=-450 ! mix. \
   videotestsrc pattern=0 ! video/x-raw, framerate=10/1, width=350, height=250 ! \
     textoverlay font-desc="Sans 24" text="CAM1" valignment=top halignment=left shaded-background=true ! \
     videobox border-alpha=0 top=-200 left=-50 ! mix. \
