# Let's say a webcam doesn't support native 100x100 size.
# So this won't work:
#gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw, width=100, height=100 ! autovideosink

# But in can be scaled by using a filter element
gst-launch-1.0 v4l2src device=/dev/video0 ! videoscale ! video/x-raw, width=100, height=100 ! autovideosink
