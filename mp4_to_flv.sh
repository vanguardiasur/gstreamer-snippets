file=$1

# This pipeline flows like:
#
#  filesrc -> decodebin -> queue -> video -> enc -> flv -> filesink
#                \-------> queue -> audio -> enc ----/
#
#
#

GST_DEBUG=flv*:9,collectpads:9 \
gst-launch-1.0 filesrc location=$file ! decodebin name=decoder ! queue ! videoconvert ! x264enc ! flvmux name=mux ! filesink location=$file.flv decoder. ! queue ! audioconvert ! audioresample ! faac ! mux.

#						/--> etag preview
#                              /--> queue --> jpeg ----------> multifilesink
#			      /                    /-> etag video
#  filesrc -> decodebin -> tee ---> queue --> h264 --> flv --> filesink
#                \----------------> queue --> aac -----/
#

#gst-launch-1.0 -v filesrc location=$file ! decodebin name=decoder ! tee name=t ! \
#	       queue ! videoconvert ! x264enc ! \
#	       flvmux name=mux ! filesink location=$file.flv \
#	       decoder. ! queue ! audioconvert ! audioresample ! faac ! mux. \
#               t. ! queue leaky=1 ! videoconvert ! videorate ! video/x-raw,framerate=1/1 ! \
#	       jpegenc ! multifilesink location="%02d.jpeg"
